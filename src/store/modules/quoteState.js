import QuoteService from '@/services/QuoteService.js';

export default {
  namespaced: true,
  state: () => ({
    quotes: []
  }),
  mutations: {
    ADD_QUOTE(state, quote) {
      state.quotes.push(quote);
    },
    SET_QUOTES(state, quotes) {
      state.quotes = quotes;
    }
  },
  actions: {
    async addQuote({ commit }, quote) {
      try {
        commit('uploadingModule/SET_UPLOADING_STATUS', true, { root: true });
        commit('hasTriedToUploadModule/SET_TRIED_TO_UPLOAD', true, {
          root: true
        });
        const response = await QuoteService.postQuote(quote);
        if (response.status !== 201) {
          throw new Error('It was not possible to save the quote');
        }
        commit('ADD_QUOTE', quote);
      } catch (error) {
        commit(
          'errorModule/ADD_ERROR',
          { type: 'upload', msg: error.message },
          { root: true }
        );
      } finally {
        commit('uploadingModule/SET_UPLOADING_STATUS', false, { root: true });
        setTimeout(() => {
          commit('hasTriedToUploadModule/SET_TRIED_TO_UPLOAD', false, {
            root: true
          });
          commit('errorModule/CLEAR_ERROR', 'upload', { root: true });
        }, 4000);
      }
    },
    async getQuotes({ commit }) {
      try {
        commit('loadingModule/SET_LOADING_STATUS', true, { root: true });
        commit('errorModule/CLEAR_ERROR', 'download', { root: true });
        const response = await QuoteService.getAllQuotes();
        commit('SET_QUOTES', response.data);
      } catch (error) {
        commit(
          'errorModule/ADD_ERROR',
          { type: 'download', msg: error.message },
          { root: true }
        );
      } finally {
        commit('loadingModule/SET_LOADING_STATUS', false, { root: true });
      }
    }
  },
  getters: {
    getCurrentViewQuotes(state, getters, rootState) {
      const filters = [
        (quote) => quote.lang === 'en',
        (quote) => quote,
        (quote) => quote.lang === 'it'
      ];
      const filter = filters[rootState.currentViewModule.currentView];
      return state.quotes.filter(filter);
    }
  }
};
