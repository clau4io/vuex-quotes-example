export default {
  namespaced: true,
  state: () => ({
    uploading: false
  }),
  mutations: {
    SET_UPLOADING_STATUS(state, uploadingStatus) {
      state.uploading = uploadingStatus;
    }
  }
};
