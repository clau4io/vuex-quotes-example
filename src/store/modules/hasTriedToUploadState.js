export default {
  namespaced: true,
  state: () => ({
    hasTriedToUpload: false
  }),
  mutations: {
    SET_TRIED_TO_UPLOAD(state, value) {
      state.hasTriedToUpload = value;
    }
  }
};
