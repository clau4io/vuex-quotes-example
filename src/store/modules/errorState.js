export default {
  namespaced: true,
  state: () => ({
    errors: {
      upload: null,
      download: null
    }
  }),
  mutations: {
    ADD_ERROR(state, error) {
      state.errors[error.type] = error.msg;
    },
    CLEAR_ERROR(state, type) {
      state.errors[type] = null;
    }
  }
};
