export default {
  namespaced: true,
  state: () => ({
    editingMode: false
  }),
  mutations: {
    SET_EDITING_MODE(state, editingMode) {
      state.editingMode = editingMode;
    }
  },
  actions: {
    setEditingMode({ commit }, value) {
      commit('SET_EDITING_MODE', value);
    }
  }
};
