export default {
  namespaced: true,
  state: () => ({
    currentView: 1
  }),
  mutations: {
    SET_CURRENT_VIEW(state, currentView) {
      state.currentView = currentView;
    }
  },
  actions: {
    setCurrentView({ commit }, currentView) {
      commit('SET_CURRENT_VIEW', currentView);
    }
  }
};
