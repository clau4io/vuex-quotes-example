export default {
  namespaced: true,
  state: () => ({
    loading: false
  }),
  mutations: {
    SET_LOADING_STATUS(state, value) {
      state.loading = value;
    }
  },
  actions: {
    setLoadingStatus({ commit }, value) {
      commit('ET_LOADING_STATUS', value);
    }
  }
};
