import Vue from 'vue';
import Vuex from 'vuex';

import currentViewModule from '@/store/modules/currentViewState';
import editingModeModule from '@/store/modules/editingModeState';
import errorModule from '@/store/modules/errorState';
import hasTriedToUploadModule from '@/store/modules/hasTriedToUploadState';
import loadingModule from '@/store/modules/loadingState';
import uploadingModule from '@/store/modules/uploadingState';
import quoteModule from '@/store/modules/quoteState';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    currentViewModule,
    editingModeModule,
    errorModule,
    hasTriedToUploadModule,
    loadingModule,
    uploadingModule,
    quoteModule
  }
});
