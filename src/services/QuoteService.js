import axios from 'axios';

const { VUE_APP_REMOTE_ADDRESS: ADDRESS, VUE_APP_PORT: PORT } = process.env;

const axiosInstance = axios.create({
  baseURL: `//${ADDRESS}:${PORT}`,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
});

export default {
  getAllQuotes() {
    return axiosInstance.get('/quotes');
  },
  getQuote(id) {
    return axiosInstance.get(`/quotes/${id}`);
  },
  postQuote(quote) {
    return axiosInstance.post(`/quotes`, quote);
  }
};
