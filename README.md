# QuotesDB Vuex Example

Esempio di applicazione per la visualizzazione di citazioni e frasi celebri realizzata in Vue/Vuex
per la lezione 20 della guida a Vue.js disponibile su [Mr. Webmaster.it](https://www.mrwebmaster.it)

![Vuex Quotes db example screenshot](https://bitbucket.org/clau4io/vuex-quotes-example/raw/d8160619e189a71cfb76e0b246a00684873c1cbb/screenshots/vuex-quotes-db-example-screenshot.png)

## Istruzioni

Dopo essersi assicurati di aver installato Node.js `node --version`, scaricare il repository.

```
git clone https://clau4io@bitbucket.org/clau4io/vuex-quotes-example.git
```

Spostarsi nella nuova cartella ed eseguire il comando `npm install` per installare
tutte le dipendenze.

### Avviare server locale durante la fase di sviluppo

Lanciare il comando `npm start` per avviare il server locale e JSON server che è stato installato
localmente fra le dipendenze.

